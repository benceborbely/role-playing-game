package com.benceborbely.rpg.business;

public enum Direction {
    NORTH,
    SOUTH,
    WEST,
    EAST
}
