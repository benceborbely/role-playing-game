package com.benceborbely.rpg.business;

import java.io.Serializable;

public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    private int targetDistance;

    public Task(int targetDistance) {
        if (targetDistance <= 0) {
            throw new IllegalArgumentException("targetDistance must be larger than 0!");
        }

        this.targetDistance = targetDistance;
    }

    public int getTargetDistance() {
        return targetDistance;
    }

    public void increaseProgress() {
        targetDistance--;
    }

    public void decreaseProgress() {
        targetDistance++;
    }

    public boolean isCompleted() {
        return targetDistance == 0;
    }

}
