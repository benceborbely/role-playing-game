package com.benceborbely.rpg.business;

import java.io.Serializable;

public class Player implements Serializable {

    private static final long serialVersionUID = 1L;

    private int experience;

    public Player() {
        this.experience = 0;
    }

    public void increaseExperience() {
        experience++;
    }

    public int getExperience() {
        return experience;
    }

}
