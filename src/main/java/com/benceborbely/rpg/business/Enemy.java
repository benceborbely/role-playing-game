package com.benceborbely.rpg.business;

import java.io.Serializable;

public class Enemy implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    public Enemy(String name) {
        if (name == null || name.trim().isEmpty()) {
            throw new IllegalArgumentException("name cannot be blank.");
        }

        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Enemy enemy = (Enemy) o;

        return name != null ? name.equals(enemy.name) : enemy.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

}
