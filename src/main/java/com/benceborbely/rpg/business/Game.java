package com.benceborbely.rpg.business;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class Game implements Serializable {

    private static final long serialVersionUID = 1L;

    private Player player;
    private Task task;
    private List<Character> characters;
    private List<Enemy> enemies;
    private Random random;
    private Enemy enemy;
    private Direction targetDirection;

    public Game(Player player, Task task, List<Character> characters, List<Enemy> enemies, Random random) {
        if (player == null) {
            throw new IllegalArgumentException("player cannot be null.");
        }

        if (task == null) {
            throw new IllegalArgumentException("task cannot be null.");
        }

        if (characters == null || characters.isEmpty()) {
            throw new IllegalArgumentException("characters cannot be null or empty.");
        }

        if (enemies == null || enemies.isEmpty()) {
            throw new IllegalArgumentException("enemies cannot be null or empty.");
        }

        if (random == null) {
            throw new IllegalArgumentException("random cannot be null.");
        }

        this.player = player;
        this.task = task;
        this.characters = characters;
        this.enemies = enemies;
        this.random = random;
        this.enemy = null;
        this.targetDirection = null;
    }

    public Optional<Enemy> explore(Direction direction) {
        if (enemy != null) {
            throw new IllegalStateException("Cannot explore until enemy is present.");
        }

        targetDirection = direction;

        if (Direction.SOUTH == targetDirection) {
            task.decreaseProgress();
            return Optional.empty();
        }

        enemy = random.nextBoolean() ? enemies.get(random.nextInt(enemies.size())) : null;

        if (Direction.NORTH == targetDirection && enemy == null) {
            task.increaseProgress();
            targetDirection = null;
        }

        return Optional.ofNullable(enemy);
    }

    public boolean fight() {
        if (enemy == null) {
            throw new IllegalStateException("Cannot fight until enemy is not present.");
        }

        boolean isPlayerWon = random.nextBoolean();

        if (isPlayerWon && targetDirection == Direction.NORTH) {
            task.increaseProgress();
        }

        enemy = null;
        targetDirection = null;
        player.increaseExperience();
        return isPlayerWon;
    }

    public void escape() {
        if (enemy == null) {
            throw new IllegalStateException("Cannot escape until enemy is not present.");
        }

        enemy = null;
        targetDirection = null;
        task.decreaseProgress();
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public Task getTask() {
        return task;
    }

    public Player getPlayer() {
        return player;
    }

    public static GameBuilder builder() {
        return new GameBuilder();
    }

    public static final class GameBuilder {

        private Player player;
        private Task task;
        private List<Character> characters;
        private List<Enemy> enemies;
        private Random random;

        public GameBuilder player(Player player) {
            this.player = player;
            return this;
        }

        public GameBuilder task(Task task) {
            this.task = task;
            return this;
        }

        public GameBuilder characters(List<Character> characters) {
            this.characters = characters;
            return this;
        }

        public GameBuilder enemies(List<Enemy> enemies) {
            this.enemies = enemies;
            return this;
        }

        public GameBuilder random(Random random) {
            this.random = random;
            return this;
        }

        public Game build() {
            return new Game(player, task, characters, enemies, random);
        }

    }

}
