package com.benceborbely.rpg.business.service;

import com.benceborbely.rpg.business.Character;
import com.benceborbely.rpg.business.Enemy;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface GameService {

    void createGame();

    Optional<Enemy> moveToNorth();

    Optional<Enemy> moveToSouth();

    Optional<Enemy> moveToEast();

    Optional<Enemy> moveToWest();

    boolean fight();

    void escape();

    int getProgress();

    int getExperience();

    boolean isCompleted();

    List<Character> getCharacters();

    String save() throws IOException;

    void load(String code) throws IOException, ClassNotFoundException;

}
