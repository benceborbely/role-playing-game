package com.benceborbely.rpg.business.service.impl;

import com.benceborbely.rpg.business.*;
import com.benceborbely.rpg.business.Character;
import com.benceborbely.rpg.business.service.GameService;

import java.io.*;
import java.util.*;

public class GameServiceImpl implements GameService {

    private static final String FILE_EXT = ".ser";

    private Game game;

    @Override
    public void createGame() {
        List<Character> characters = new ArrayList<>();
        characters.add(new Character("John Snow", "Knowing Nothing"));
        characters.add(new Character("Tyrion Lannister", "Drinking to death"));
        characters.add(new Character("Daenerys Targaryen", "The mother of dragons"));
        characters.add(new Character("Arya Stark", "No one"));

        ArrayList<Enemy> enemies = new ArrayList<>();
        enemies.add(new Enemy("White Walker"));
        enemies.add(new Enemy("Wight"));
        enemies.add(new Enemy("Ice Dragon"));

        game = Game.builder()
                .characters(characters)
                .enemies(enemies)
                .task(new Task(5))
                .player(new Player())
                .random(new Random())
                .build();
    }

    @Override
    public Optional<Enemy> moveToNorth() {
        return game.explore(Direction.NORTH);
    }

    @Override
    public Optional<Enemy> moveToSouth() {
        return game.explore(Direction.SOUTH);
    }

    @Override
    public Optional<Enemy> moveToEast() {
        return game.explore(Direction.EAST);
    }

    @Override
    public Optional<Enemy> moveToWest() {
        return game.explore(Direction.WEST);
    }

    @Override
    public boolean fight() {
        return game.fight();
    }

    @Override
    public void escape() {
        game.escape();
    }

    @Override
    public int getProgress() {
        return game.getTask().getTargetDistance();
    }

    @Override
    public int getExperience() {
        return game.getPlayer().getExperience();
    }

    @Override
    public boolean isCompleted() {
        return game.getTask().isCompleted();
    }

    @Override
    public List<Character> getCharacters() {
        return game.getCharacters();
    }

    @Override
    public String save() throws IOException {
        String filename = String.valueOf(System.currentTimeMillis());
        FileOutputStream fileOutputStream = new FileOutputStream(filename + FILE_EXT);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(game);
        objectOutputStream.flush();
        objectOutputStream.close();
        return filename;
    }

    @Override
    public void load(String code) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(code + FILE_EXT);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        game = (Game) objectInputStream.readObject();
        objectInputStream.close();
    }

}
