package com.benceborbely.rpg.business;

import java.io.*;

public class Character implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String skill;

    public Character(String name, String skill) {
        if (name == null || name.trim().isEmpty()) {
            throw new IllegalArgumentException("name cannot be blank.");
        }

        if (skill == null || skill.trim().isEmpty()) {
            throw new IllegalArgumentException("skill cannot be blank.");
        }

        this.name = name;
        this.skill = skill;
    }

    public String getName() {
        return name;
    }

    public String getSkill() {
        return skill;
    }

}
