package com.benceborbely.rpg.cli;

import com.benceborbely.rpg.business.service.GameService;
import com.benceborbely.rpg.business.service.impl.GameServiceImpl;
import com.benceborbely.rpg.cli.command.*;

import java.util.*;

public class CommandLine {

    public void start() {
        try {
            Scanner scanner = new Scanner(System.in);
            GameService gameService = new GameServiceImpl();

            RootCommand rootCommand = new RootCommand(gameService, scanner);
            MainMenuCommand mainMenuCommand = new MainMenuCommand(gameService, scanner);
            CreateCharacterCommand createCharacterCommand = new CreateCharacterCommand(gameService, scanner);
            NewGameCommand newGameCommand = new NewGameCommand(gameService, scanner);
            ExitCommand exitCommand = new ExitCommand(gameService, scanner);
            ExploreCommand exploreCommand = new ExploreCommand(gameService, scanner);
            ExitFromExplorationCommand exitFromExplorationCommand = new ExitFromExplorationCommand(gameService, scanner);
            MoveNorthCommand moveNorthCommand = new MoveNorthCommand(gameService, scanner);
            MoveWestCommand moveWestCommand = new MoveWestCommand(gameService, scanner);
            MoveToEastCommand moveEastCommand = new MoveToEastCommand(gameService, scanner);
            MoveToSouthCommand moveToSouthCommand = new MoveToSouthCommand(gameService, scanner);
            FightCommand fightCommand = new FightCommand(gameService, scanner);
            StepBackCommand stepBackCommand = new StepBackCommand(gameService, scanner);
            SaveCommand saveCommand = new SaveCommand(gameService, scanner);
            LoadGameCommand loadGameCommand = new LoadGameCommand(gameService, scanner);

            rootCommand.addSubCommand(newGameCommand);
            rootCommand.addSubCommand(loadGameCommand);
            rootCommand.addSubCommand(exitCommand);

            newGameCommand.addSubCommand(createCharacterCommand);
            newGameCommand.addSubCommand(mainMenuCommand);

            loadGameCommand.addSubCommand(exploreCommand);

            createCharacterCommand.addSubCommand(exploreCommand);

            exploreCommand.addSubCommand(moveNorthCommand);
            exploreCommand.addSubCommand(moveEastCommand);
            exploreCommand.addSubCommand(moveWestCommand);
            exploreCommand.addSubCommand(moveToSouthCommand);
            exploreCommand.addSubCommand(saveCommand);
            exploreCommand.addSubCommand(exitFromExplorationCommand);

            moveNorthCommand.addSubCommand(fightCommand);
            moveNorthCommand.addSubCommand(stepBackCommand);

            moveEastCommand.addSubCommand(fightCommand);
            moveEastCommand.addSubCommand(stepBackCommand);

            moveToSouthCommand.addSubCommand(fightCommand);
            moveToSouthCommand.addSubCommand(stepBackCommand);

            moveWestCommand.addSubCommand(fightCommand);
            moveWestCommand.addSubCommand(stepBackCommand);

            rootCommand.execute();
        } catch (Exception e) {
            //todo log the fatal error
            System.out.println(Text.FATAL_ERROR);
        }
    }

}
