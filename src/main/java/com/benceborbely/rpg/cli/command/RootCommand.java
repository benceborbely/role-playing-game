package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.service.GameService;
import com.benceborbely.rpg.cli.Text;

import java.util.Scanner;

public class RootCommand extends AbstractCommand {

    public RootCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    public boolean match(String input) {
        return false;
    }

    public void execute() {
        System.out.println(Text.TITLE);
        System.out.println(Text.NAVIGATION_INSTRUCTION);

        while (true) {
            System.out.println(Text.MAIN_MENU);
            System.out.println(Text.AVAILABLE_OPTIONS);
            System.out.println(Text.options(subCommands));

            Command command = readUntilNotValid(scanner.nextLine());
            command.execute();
        }
    }

    public String getName() {
        return null;
    }

}
