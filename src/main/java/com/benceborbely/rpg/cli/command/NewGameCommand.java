package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.service.GameService;
import com.benceborbely.rpg.cli.Text;

import java.util.*;

public class NewGameCommand extends AbstractCommand {

    public NewGameCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    public void execute() {
        System.out.println(Text.NEW_GAME_MENU);
        System.out.println(Text.AVAILABLE_OPTIONS);
        System.out.println(Text.options(subCommands));

        gameService.createGame();

        String input = scanner.nextLine();

        Command command = readUntilNotValid(input);

        command.execute();
    }

    @Override
    public String getName() {
        return "new game";
    }

}
