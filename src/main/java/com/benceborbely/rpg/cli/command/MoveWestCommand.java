package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.Enemy;
import com.benceborbely.rpg.business.service.GameService;

import java.util.Optional;
import java.util.Scanner;

public class MoveWestCommand extends MoveCommand {

    public MoveWestCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    protected Optional<Enemy> move() {
        return gameService.moveToWest();
    }

    @Override
    public String getName() {
        return "move to west";
    }

}
