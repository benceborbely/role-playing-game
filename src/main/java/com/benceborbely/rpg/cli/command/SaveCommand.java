package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.service.GameService;
import com.benceborbely.rpg.cli.Text;

import java.util.Scanner;

public class SaveCommand extends AbstractCommand {

    public SaveCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    public void execute() {
        try {
            String code = gameService.save();
            System.out.println(Text.GAME_SAVED_SUCCESSFULLY + code);
        } catch (Exception e) {
            System.out.println(Text.GAME_SAVING_ERROR);
        }
    }

    @Override
    public String getName() {
        return "save";
    }

}
