package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.service.GameService;
import com.benceborbely.rpg.cli.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class AbstractCommand implements Command {

    protected GameService gameService;

    protected Scanner scanner;

    protected List<Command> subCommands;

    public AbstractCommand(GameService gameService, Scanner scanner) {
        this.gameService = gameService;
        this.scanner = scanner;
        this.subCommands = new ArrayList<>();
    }

    @Override
    public abstract void execute();

    @Override
    public abstract String getName();

    public void addSubCommand(Command command) {
        subCommands.add(command);
    }

    public boolean match(String input) {
        return getName().equals(input);
    }

    protected Command readUntilNotValid(String input) {
        Command command = null;

        while (command == null) {
            command = findBy(input);

            if (command == null) {
                System.out.println(Text.INVALID_OPTION);
                input = scanner.nextLine();
            }
        }

        return command;
    }

    private Command findBy(String input) {
        return subCommands.stream().filter((c) -> c.match(input)).findFirst().orElse(null);
    }

}
