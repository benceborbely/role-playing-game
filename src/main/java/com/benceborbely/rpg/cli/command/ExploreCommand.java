package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.service.GameService;
import com.benceborbely.rpg.cli.Text;

import java.util.Scanner;

public class ExploreCommand extends AbstractCommand {

    public ExploreCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    public void execute() {
        System.out.println(Text.MISSION_STARTED);
        System.out.println(Text.EXPLORATION_OPTIONS);
        System.out.println(Text.options(subCommands));
        System.out.println(Text.xpLevel(gameService.getExperience()));
        System.out.println(Text.progressMsg(gameService.getProgress()));

        while (!gameService.isCompleted()) {
            System.out.print(Text.MISSION_INSTRUCTION);
            Command command = readUntilNotValid(scanner.nextLine());

            if (command == null) {
                System.out.println(Text.INVALID_OPTION);
            } else {
                command.execute();
            }
        }

        System.out.println(Text.GAME_OVER);
    }

    @Override
    public String getName() {
        return "explore";
    }

}
