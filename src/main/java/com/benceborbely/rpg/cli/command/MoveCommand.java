package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.Enemy;
import com.benceborbely.rpg.business.service.GameService;
import com.benceborbely.rpg.cli.Text;

import java.util.Optional;
import java.util.Scanner;

public abstract class MoveCommand extends AbstractCommand {

    public MoveCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    public void execute() {
        Optional<Enemy> enemy = move();

        if (enemy.isPresent()) {
            System.out.println(Text.enemyMsg(enemy.get().getName()));
            Command command = readUntilNotValid(scanner.nextLine());
            command.execute();
        } else {
            System.out.println(Text.NO_ENEMY);
        }

        System.out.println(Text.xpLevel(gameService.getExperience()));
        System.out.println(Text.progressMsg(gameService.getProgress()));
    }

    protected abstract Optional<Enemy> move();

}
