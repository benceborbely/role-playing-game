package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.service.GameService;
import com.benceborbely.rpg.cli.Text;

import java.util.Scanner;

public class FightCommand extends AbstractCommand {

    public FightCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    public void execute() {
        if (gameService.fight()) {
            System.out.print(Text.WIN_MSG);
        } else {
            System.out.print(Text.LOST_MSG);
        }
    }

    @Override
    public String getName() {
        return "fight";
    }

}
