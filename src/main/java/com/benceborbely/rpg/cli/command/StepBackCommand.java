package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.service.GameService;
import com.benceborbely.rpg.cli.Text;

import java.util.Scanner;

public class StepBackCommand extends AbstractCommand {

    public StepBackCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    public void execute() {
        gameService.escape();
        System.out.println(Text.ESCAPED);
    }

    @Override
    public String getName() {
        return "step back";
    }

}
