package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.Enemy;
import com.benceborbely.rpg.business.service.GameService;

import java.util.Optional;
import java.util.Scanner;

public class MoveNorthCommand extends MoveCommand {

    public MoveNorthCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    protected Optional<Enemy> move() {
        return gameService.moveToNorth();
    }

    @Override
    public String getName() {
        return "move to north";
    }

}
