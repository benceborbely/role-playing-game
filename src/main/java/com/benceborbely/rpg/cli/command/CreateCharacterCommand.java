package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.Character;
import com.benceborbely.rpg.business.service.GameService;
import com.benceborbely.rpg.cli.Text;

import java.util.List;
import java.util.Scanner;

public class CreateCharacterCommand extends AbstractCommand {

    public CreateCharacterCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    public void execute() {
        List<Character> characters = gameService.getCharacters();

        System.out.println(Text.CREATE_CHARACTER_MENU);
        System.out.println(Text.CHOOSE_CHARACTER);
        System.out.println(Text.listCharacters(characters));

        String input = scanner.nextLine();

        Character character = null;

        while (character == null) {
            character = findBy(input, characters);

            if (character == null) {
                System.out.println(Text.INVALID_OPTION);
                input = scanner.nextLine();
            }
        }

        System.out.println(Text.TASK_DESCRIPTION);
        System.out.println(Text.options(subCommands));

        Command command = readUntilNotValid(scanner.nextLine());
        command.execute();
    }

    @Override
    public boolean match(String input) {
        return getName().equals(input);
    }

    @Override
    public String getName() {
        return "create character";
    }

    private Character findBy(String name, List<Character> characters) {
        return characters
                .stream()
                .filter((c) -> c.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

}
