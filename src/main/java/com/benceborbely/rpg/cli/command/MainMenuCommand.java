package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.service.GameService;

import java.util.Scanner;

public class MainMenuCommand extends AbstractCommand {

    public MainMenuCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    public void execute() {

    }

    @Override
    public String getName() {
        return "main menu";
    }

}
