package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.service.GameService;

import java.util.Scanner;

public class ExitFromExplorationCommand extends ExitCommand {

    public ExitFromExplorationCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    public void execute() {
        super.execute();
    }

    @Override
    public String getName() {
        return "exit";
    }

}
