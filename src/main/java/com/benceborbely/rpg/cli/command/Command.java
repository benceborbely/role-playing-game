package com.benceborbely.rpg.cli.command;

public interface Command {

    boolean match(String input);

    void execute();

    String getName();

}
