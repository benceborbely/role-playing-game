package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.service.GameService;
import com.benceborbely.rpg.cli.Text;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class LoadGameCommand extends AbstractCommand {

    public LoadGameCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    public void execute() {
        System.out.print(Text.ENTER_GAME_CODE);
        String code = scanner.nextLine();

        try {
            gameService.load(code);

            System.out.println(Text.GAME_LOADED_SUCCESSFULLY);
            System.out.println(Text.options(subCommands));

            Command command = readUntilNotValid(scanner.nextLine());
            command.execute();
        } catch (FileNotFoundException e) {
            System.out.println(Text.GAME_NOT_FOUND);
        } catch (Exception e) {
            System.out.println(Text.GAME_LOADING_ERROR);
        }
    }

    @Override
    public String getName() {
        return "load game";
    }

}
