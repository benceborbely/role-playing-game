package com.benceborbely.rpg.cli.command;

import com.benceborbely.rpg.business.service.GameService;

import java.util.Scanner;

public class ExitCommand extends AbstractCommand {

    public ExitCommand(GameService gameService, Scanner scanner) {
        super(gameService, scanner);
    }

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String getName() {
        return "exit";
    }

}
