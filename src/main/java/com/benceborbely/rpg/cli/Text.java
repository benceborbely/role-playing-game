package com.benceborbely.rpg.cli;

import com.benceborbely.rpg.business.Character;
import com.benceborbely.rpg.cli.command.Command;

import java.util.List;

public class Text {

    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_RESET = "\u001B[0m";

    public static final String NEW_LINE = "\n";

    public static final String TITLE = ANSI_BLUE + "*** Game of Thrones ***" + ANSI_RESET;
    public static final String NAVIGATION_INSTRUCTION = NEW_LINE + "You can navigate in the game by typing in the words listed under the available options.";
    public static final String MAIN_MENU = NEW_LINE + "--Main menu--";
    public static final String NEW_GAME_MENU = NEW_LINE + "--New game menu--";
    public static final String CREATE_CHARACTER_MENU = NEW_LINE + "--Create character menu--";
    public static final String MISSION_STARTED = NEW_LINE + "--Mission started--";
    public static final String TASK_DESCRIPTION = NEW_LINE + "Your mission is to reach The Wall of North of the Seven Kingdoms and fight against the enemies who will try to stop you on your way." + NEW_LINE + "Are you ready? If yes type in the following command please: ";
    public static final String EXPLORATION_OPTIONS = "You can use the following commands to navigate and manage the game:";
    public static final String AVAILABLE_OPTIONS = "Available options:";
    public static final String INVALID_OPTION = "Please choose a valid option!";
    public static final String CHOOSE_CHARACTER = "Please choose a character by entering its name:";
    public static final String WIN_MSG = ANSI_GREEN + "You won!" + ANSI_RESET + NEW_LINE;
    public static final String LOST_MSG = ANSI_RED + "You lost!" + ANSI_RESET + NEW_LINE;
    public static final String ESCAPED = ANSI_GREEN + "Stepped back!" + ANSI_GREEN;
    public static final String NO_ENEMY = ANSI_GREEN + "No enemy!" + ANSI_RESET;
    public static final String ENTER_GAME_CODE = "Please enter the code of the game: ";
    public static final String MISSION_INSTRUCTION = "Please type in what you would like to do: ";
    public static final String GAME_SAVED_SUCCESSFULLY = "Game has been saved successfully. You can load this status of the game by entering the following code: ";
    public static final String GAME_SAVING_ERROR = "Could not save the game. Please try again!";
    public static final String GAME_LOADED_SUCCESSFULLY = "Game has been loaded! To continue the mission type in the following command please: ";
    public static final String GAME_NOT_FOUND = "Could not find a game with the code you provided.";
    public static final String GAME_LOADING_ERROR = "Some error has happened while loading the game. Please try again!";
    public static final String GAME_OVER = ANSI_GREEN + "*** You have reached the Wall and survived! Congratulations! ***" + ANSI_RESET + NEW_LINE;
    public static final String FATAL_ERROR = "Sorry but some error happened and could not recover. Please try to restart the game!";

    public static String options(List<Command> options) {
        StringBuilder sb = new StringBuilder();

        options.forEach((c) -> sb.append("* ").append(c.getName()).append("\n"));

        return sb.toString();
    }

    public static String listCharacters(List<Character> characters) {
        StringBuilder sb = new StringBuilder();

        characters.forEach((c) -> sb.append(describeCharacter(c)).append("\n"));

        return sb.toString();
    }

    public static String describeCharacter(Character character) {
        return "* " + character.getName() + " - Super skill: " + character.getSkill();
    }

    public static String enemyMsg(String enemyName) {
        return "There is a(n) " + enemyName + ". Do you want to 'fight' or 'step back'?";
    }

    public static String xpLevel(int level) {
        return ANSI_BLUE + "XP level: " + level + ANSI_RESET;
    }

    public static String progressMsg(int progress) {
        return  ANSI_BLUE + "You need to take " + progress + " step(s) to the Wall!" + ANSI_RESET;
    }

}
