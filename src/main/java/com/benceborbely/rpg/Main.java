package com.benceborbely.rpg;

import com.benceborbely.rpg.cli.CommandLine;

public class Main {

    public static void main(String[] args) {
        CommandLine commandLine = new CommandLine();
        commandLine.start();
    }

}
