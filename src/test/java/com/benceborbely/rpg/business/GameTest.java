package com.benceborbely.rpg.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {

    @Mock
    private Task task;

    @Mock
    private Player player;

    @Mock
    private List<Character> characters;

    @Mock
    private List<Enemy> enemies;

    @Mock
    private Random random;

    @InjectMocks
    private Game underTest;

    @Before
    public void setUp() throws Exception {
        underTest = Game.builder()
                .task(task)
                .player(player)
                .characters(characters)
                .enemies(enemies)
                .random(random)
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfPlayerIsNull() {
        new Game(null, task, characters, enemies, random);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfTaskIsNull() {
        new Game(player, null, characters, enemies, random);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfCharactersIsNull() {
        new Game(player, task, null, enemies, random);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfCharactersIsEmpty() {
        new Game(player, task, new ArrayList<>(), enemies, random);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfEnemiesIsNull() {
        when(characters.isEmpty()).thenReturn(false);

        new Game(player, task, characters, null, random);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfEnemiesIsEmpty() {
        when(characters.isEmpty()).thenReturn(false);

        new Game(player, task, characters, new ArrayList<>(), random);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfRandomIsNull() {
        when(characters.isEmpty()).thenReturn(false);
        when(enemies.isEmpty()).thenReturn(false);

        new Game(player, task, characters, enemies, null);
    }

    @Test
    public void testMoveToSouth() {
        Optional<Enemy> enemy = underTest.explore(Direction.SOUTH);

        assertFalse(enemy.isPresent());
        verify(task, times(1)).decreaseProgress();
        verify(task, never()).increaseProgress();
    }

    @Test
    public void testMoveToNorthWhenEnemyIsPresent() {
        testMoveWhenEnemyIsPresent(Direction.NORTH);
    }

    @Test
    public void testMoveToNorthWhenEnemyIsNotPresent() {
        testMoveWhenEnemyIsNotPresent(Direction.NORTH);
        verify(task, times(1)).increaseProgress();
    }

    @Test
    public void testMoveToWestWhenEnemyIsPresent() {
        testMoveWhenEnemyIsPresent(Direction.WEST);
    }

    @Test
    public void testMoveToWestWhenEnemyIsNotPresent() {
        testMoveWhenEnemyIsNotPresent(Direction.WEST);
        verify(task, never()).increaseProgress();
    }

    @Test
    public void testMoveToEastWhenEnemyIsPresent() {
        testMoveWhenEnemyIsPresent(Direction.EAST);
    }

    @Test
    public void testMoveToEastWhenEnemyIsNotPresent() {
        testMoveWhenEnemyIsNotPresent(Direction.EAST);
        verify(task, never()).increaseProgress();
    }

    @Test(expected = IllegalStateException.class)
    public void testEscapeWhenEnemyIsNotPresent() {
        underTest.escape();
    }

    @Test
    public void testEscapeWhenEnemyIsPresent() {
        when(random.nextBoolean()).thenReturn(true);
        when(enemies.get(anyInt())).thenReturn(new Enemy("White Walker"));

        underTest.explore(Direction.NORTH);
        underTest.escape();

        verify(task, times(1)).decreaseProgress();
        verify(task, never()).increaseProgress();
    }

    @Test(expected = IllegalStateException.class)
    public void testFightWhenEnemyIsNotPresent() {
        underTest.fight();
    }

    @Test
    public void testFightWhenEnemyIsPresentAndPlayerWins() {
        when(random.nextBoolean())
                .thenReturn(true)
                .thenReturn(true);
        when(enemies.get(anyInt())).thenReturn(new Enemy("White Walker"));

        underTest.explore(Direction.NORTH);
        underTest.fight();

        verify(task, times(1)).increaseProgress();
        verify(task, never()).decreaseProgress();
        verify(player, times(1)).increaseExperience();
    }

    @Test
    public void testFightWhenEnemyIsPresentAndEnemyWins() {
        when(random.nextBoolean())
                .thenReturn(true)
                .thenReturn(false);
        when(enemies.get(anyInt())).thenReturn(new Enemy("White Walker"));

        underTest.explore(Direction.NORTH);
        underTest.fight();

        verify(task, never()).decreaseProgress();
        verify(task, never()).increaseProgress();
        verify(player, times(1)).increaseExperience();
    }

    @Test(expected = IllegalStateException.class)
    public void testMoveWhenEnemyIsPresent() {
        when(random.nextBoolean()).thenReturn(true);
        when(enemies.get(anyInt())).thenReturn(new Enemy("White Walker"));

        underTest.explore(Direction.NORTH);
        underTest.explore(Direction.WEST);
    }

    private void testMoveWhenEnemyIsNotPresent(Direction direction) {
        when(random.nextBoolean()).thenReturn(false);
        
        Optional<Enemy> enemy = underTest.explore(direction);

        assertFalse(enemy.isPresent());
        verify(task, never()).decreaseProgress();
    }

    private void testMoveWhenEnemyIsPresent(Direction direction) {
        Enemy enemy = new Enemy("White Walker");
        
        when(random.nextBoolean()).thenReturn(true);
        when(enemies.get(anyInt())).thenReturn(new Enemy("White Walker"));

        Optional<Enemy> resultedEnemy = underTest.explore(direction);

        assertTrue(resultedEnemy.isPresent());
        assertEquals(enemy, resultedEnemy.get());
        verify(task, never()).increaseProgress();
        verify(task, never()).decreaseProgress();
    }

}
