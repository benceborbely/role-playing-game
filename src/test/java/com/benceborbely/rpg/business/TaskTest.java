package com.benceborbely.rpg.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.benceborbely.rpg.business.Task;
import org.junit.Test;

public class TaskTest {

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidZeroTargetDistance() {
        new Task(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidNegativeTargetDistance() {
        new Task(-1);
    }

    @Test
    public void testIncreaseProgress() {
        Task underTest = new Task(1);

        underTest.increaseProgress();

        assertEquals(0, underTest.getTargetDistance());
        assertTrue(underTest.isCompleted());
    }

    @Test
    public void testDecreaseProgress() {
        Task underTest = new Task(1);

        underTest.decreaseProgress();

        assertEquals(2, underTest.getTargetDistance());
        assertFalse(underTest.isCompleted());
    }

}
