package com.benceborbely.rpg.business;

import org.junit.Test;

public class CharacterTest {

    private static final String NAME = "some name";
    private static final String SKILL = "some skill";
    private static final String EMPTY = "";
    private static final String WHITESPACE = " ";

    @Test(expected = IllegalArgumentException.class)
    public void testIfNameIsNull() {
        new Character(null, SKILL);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfNameIsEmpty() {
        new Character(EMPTY, SKILL);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfNameIsWhiteSpace() {
        new Character(WHITESPACE, SKILL);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfSkillIsNull() {
        new Character(NAME, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfSkillIsEmpty() {
        new Character(NAME, EMPTY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfSkillIsWhiteSpace() {
        new Character(NAME, WHITESPACE);
    }

}
