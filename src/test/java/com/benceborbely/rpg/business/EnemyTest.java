package com.benceborbely.rpg.business;

import org.junit.Test;

import static org.junit.Assert.*;

public class EnemyTest {

    private static final String NAME_WHITE_WALKER = "White Walker";
    private static final String NAME_ICE_DRAGON = "Ice Dragon";

    private Enemy underTest;

    @Test(expected = IllegalArgumentException.class)
    public void testIfNameIsNull() {
        new Enemy(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfNameIsEmpty() {
        new Enemy("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfNameIsWhitespace() {
        new Enemy(" ");
    }

    @Test
    public void testEqualsIfNamesAreDifferent() {
        assertFalse(new Enemy(NAME_WHITE_WALKER).equals(new Enemy(NAME_ICE_DRAGON)));
    }

    @Test
    public void testEqualsIfNamesAreTheSame() {
        assertTrue(new Enemy(NAME_WHITE_WALKER).equals(new Enemy(NAME_WHITE_WALKER)));
    }

    @Test
    public void testEqualsIfReferencingTheSameObject() {
        Enemy enemy1 = new Enemy(NAME_WHITE_WALKER);
        Enemy enemy2 = enemy1;

        assertTrue(enemy1.equals(enemy2));
    }

    @Test
    public void testEqualsIfReferencingNull() {
        assertFalse(new Enemy(NAME_WHITE_WALKER).equals(null));
    }

}
