# Role Playing Game - Game of Thrones

## About the game

Choose your favourite character and complete the mission which is to reach The Wall of North of the Seven Kingdoms. It will be an exciting and adventurous journey that will make you facing randomly appearing mysterious creatures. The decision is yours: you fight with a hope to win, progress and get experience or you step back, escape from the scary monster and get further from the Wall.

"Brace yourself!"

## Features

* You can start a new game
* You can save the game and load it later
* You can choose your character
* You can navigate
* You can fight the enemy or escape from it

## How to build and run the application?

### Prerequisites

* Maven 3
* Java 8

### Build

You can build the application by running the following command in the project root:

    mvn clean package

After the build Jacoco test coverage report will be available under *target/site/jacoco* directory.

### Run

You can run the application by running the following command in the project root:

    java -jar target/rpg-1.0-SNAPSHOT.jar